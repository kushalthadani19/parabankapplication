/**
 * 
 */
package com.driver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

/**
 * @author Kushal Thadani
 *
 */
public class Config {

	private HashMap<String, String> value = new HashMap<String, String>();
	public HashMap<String, String> CONFIG_DATA(){
		return value;
	}
	public Config() {
		try {
			Properties p = new Properties();
			FileInputStream s = new FileInputStream("config.properties");
			p.load(s);
			Enumeration<?> e = p.propertyNames();
			while(e.hasMoreElements()) {
				String key = (String) e.nextElement();
				String val = p.getProperty(key);
				value.put(key, val);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}

	}
}
