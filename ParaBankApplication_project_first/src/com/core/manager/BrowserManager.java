package com.core.manager;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/*
     * @author Kushal Thadani
 */
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;

import com.driver.Config;

public class BrowserManager{

	WebDriver driver = null;
	public WebDriver getBrowser(String browser) {
		Config config = new Config();
		HashMap<String, String> config_data = config.CONFIG_DATA();
		
			
		switch(browser) {
		case "chrome":
				System.setProperty("webdriver.chrome.driver", config_data.get("CHROME_DRIVER"));
				driver=new ChromeDriver();
		case "firefox":
			System.setProperty("webdriver.chrome.driver", config_data.get("FIREFOX_DRIVER"));
			driver = new FirefoxDriver();
		case "opera":
			System.setProperty("webdriver.chrome.driver", config_data.get("OPERA_DRIVER"));
			driver = new OperaDriver();
		}
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
		return driver;
	}
	public boolean getUrl(WebDriver driver, String url) {
		driver.navigate().to("");
		return false;
	}


}
